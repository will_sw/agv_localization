#ifndef FIND_DECODE_H
#define FIND_DECODE_H
#include "common.h"
#include <iostream>
using namespace std;

extern ver_pair vps[100];
extern int L;

void findVertcialLinesFromSeg_mat(line_template seg_mat[][segNumLimit/2], int seg_cnt[], int lthThres1, int lthThres2, ver_pair vps[], int &m);
int find_LB_fromVerticalPairs(uchar image[], uint auxSubIndex[],ushort width, ushort height, ver_pair vps[], int m, int lthThres1);
void adjustAngle(uchar image[], uint auxSubIndex[], ushort width, ushort height, line_template &l, Hposition_f &lparam, uchar detDir);
bool adjustLBEndpoint(uchar image[], uint auxSubIndex[], ushort width, ushort height, ver_pair &vp);
bool verify(uchar image[], uint auxSubIndex[], ushort width, ushort height, position &dstP, uchar detDir,uchar binaryThres, uchar pointType);
void calcSegNumSegLth(uchar image[], uint auxSubIndex[], position &dstP, uchar binaryThres, uchar softSize, uchar &segNum, uchar &segLth, uchar &idx_s);
float updateL_lth(position_f &C, position_f &L, position_f &B);
void adjustLB(position &leftP, position &midP, position &bottomP, position &crossP, ver_pair &vp);
uchar calacDetDir(line_template &l, uchar quad, bool isLeftFlag);
position calacE(position S, position oldE, float lth);
void samplePoints(position &s, position &e, const uchar sampNum, const uchar sampGap, position sps[]);
void LSnewAngle(position sps[], int spsLth, line_template &l, Hposition_f &lparam);
bool DmRegionCheck(uchar image[], uint auxSubIndex[],ushort width, ushort height, ver_pair &vp, float L_lth, uchar &binaryThres, pose &POSE);
bool decode(uchar *image, float &theta, float &lth, position &crossP, pose &POSE);
uchar meanGray(uchar *image, uint *auxSubIndex, position &p);
//bool verify(uchar *image, line_template (&seg)[segNumLimit], position &crossP, float &theta, int &leftLineIdx, int lineIndex1, int lineIndex2, int lthThres1, int lthThres2);

#endif // FIND_DECODE_H
