#ifndef COMMON_H
#define COMMON_H
#include "usb.h"
#include "math.h"

//
typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned short ushort;
/******@brief
  o————————————>x
  |
  |
  |
  |
 \|/
*****************/
//2D图像坐标
struct position
{
    int col;    //col=x
    int row;    //row=y
};
struct position_f
{
    float x;    //col=x
    float y;    //row=y
};
struct Hposition
{
    int x;
    int y;
    int scale;
};
//2D图像齐次坐标
struct Hposition_f
{
    double x;
    double y;
    double scale;
};
struct pose
{
    float x;
    float y;
    float theta;
};
struct line_template
{
    position s; //起点
    position e; //终点
    float dir;
    int pixNum;
};

struct ver_pair
{
    position_f CrossP_f;
    position_f leftP_f;
    position_f bottomP_f;
    Hposition_f Lparam;
    Hposition_f Bparam;
    line_template L;
    line_template B;
    uchar LquadC;
    uchar BquadC;
};

#define imgSize 307200
#define imgRgbSize 921600
#define imgHeight 480
#define imgWidth 640

#define sampleInterval 2

#define sampledImgSize 76800
#define sampledHeight 240
#define sampledWidth 320

#define PI 3.1416

extern uchar imgData[imgSize]; //image A buff
extern uchar imgDataB[imgSize]; //image B buff

extern uchar imgRgb[imgRgbSize];

extern uchar imagePtr[sampledImgSize];
extern uchar imgMirriorSeam[sampledImgSize];
extern uchar imgMirriorSeam_copy[sampledImgSize];
extern uint imgMirriorSeam_copy_copy[sampledImgSize];

extern uint preComput[imgHeight]; //unsampled image's aided computing matrix
extern uint preComput_sampled[sampledHeight]; //sampled image's aided computing matrix
extern uint preSampleLocat[sampledImgSize]; // down sampling aided computing matrix


#define segNumLimit 1000  //line detection  result  bufff size
extern line_template line_seg[segNumLimit];
extern line_template line_seg_mat[60][segNumLimit/2]; //line detection  result  bufff
extern int line_seg_cnt[60]; //line detection number buff
extern line_template line_cell[1];
extern line_template line_sub_cell[1];
extern int seg_cnt;
extern signed char coor[8][2]; //3X3 template
extern signed char coor5X5[16][2]; //5X5 template

extern const position locP[19];
extern const position DatP[64];
extern position drawPs[19];
extern position drawDPs[64];
extern position location_point, test_point;

extern float L_lth; //side length of L of DM in image
extern uchar grayThres; //gray binary threshold of image
extern pose agvPoseFromDM; //calculated robot(agv)'s pose from DM
extern pose agvPoseFromOdometry;//calculated robot(agv)'s pose from odometry

extern int samImgCnt;
extern int decodeCnt;
extern struct timeval tvNow,tvNowNow;

extern bool m_isThreadWorkBusy; //flag of work thread busy,true is busy
extern bool currentIsAorB;  //true is A,false is B
extern bool openOdometryByVelocity;//true means open
extern bool openOdometryByDisplacement;//true means open
extern bool openLocaterByDMbar;//true means open

extern position samPS[100];

void calcPreComput();
void calcPreComput_sampled();
void calcPreSampleLocat();
void initVar();
void initLineSeg();
Hposition_f cross_fhp_fhp(Hposition_f A, Hposition_f B);
position_f cross_fhp_fp(Hposition_f A, Hposition_f B);
position cross_fhp_ip(Hposition_f A, Hposition_f B);

Hposition_f cross_ip_fhp(position A, position B);
Hposition cross_ip_hp(position A, position B);

position cross_hp_ip(Hposition A, Hposition B);
position_f cross_hp_fp(Hposition A, Hposition B);
int manhattan(position A, position B);


//////////////////////////////////////////////////////////////////////////////////////
typedef struct tagBITMAPFILEHEADER
{
    unsigned short int	bfType;//文件类型,ASCII字符BM;
    unsigned long 		bfSize;//文件大小
    unsigned short int	bfReserved1;//备用
    unsigned short int	bfReserved2;//备用
    unsigned long 		bfOffBits;//图像开始处的字节偏移
} BITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER
{
    unsigned long		biSize;//信息头大小
    long				biWidth;//图像宽度,以像素为单位
    long				biHeight;//图像高度,以像素为单位
    unsigned short int 	biPlanes;//位平面数,为1;
    unsigned short int 	biBitCount;//每像素位数,1,4,8,24
    unsigned long 		biCompression;//压缩类型,0为非压缩
    unsigned long 		biSizeImage;//压缩图像大小
    long				biXPelsPerMeter;//水平分辨率
    long				biYPelsPerMeter;//垂直分辨率
    unsigned long 		biClrUsed;//使用的色彩数
    unsigned long 		biClrImportant;//重要色彩数
} BITMAPINFOHEADER;

typedef struct tagRGBQUAD{
    unsigned char rgbBlue; //该颜色的蓝色分量
    unsigned char rgbGreen; //该颜色的绿色分量
    unsigned char rgbRed; //该颜色的红色分量
    unsigned char rgbReserved; //保留值
}RGBQUAD;//调色板定义

void bayer2rgb24_ccjt(unsigned char *dst, unsigned char *src, long width, long height);
int save_to_bmp(char *szFile, uchar* pBmp, int width, int height, int bitCount);
int read_from_bmp(char *szFile, uchar* pBmp, int width, int height);
//////////////////////////////////////////////////////////////////////////////////////
#endif // COMMON_H
