#ifndef FREEMAN_H
#define FREEMAN_H

#include "common.h"
void chaincode(uchar image[], uint auxSubIndex[], ushort width, ushort height, position &current_p, signed char &current_dir, signed char &main_dir);
void chaincodeDetectLine(uchar image[], uint auxSubIndex[], ushort width, ushort height, line_template line_seg_mat[][segNumLimit/2], int line_seg_cnt[]);

bool updateLineSubCell(line_template &sub, int &subCnt, signed char &subDir1, signed char &subDir2, position &curP, signed char &curDir);
bool updateLineCell(line_template &cell, int &cellCnt, signed char &cellDir1, signed char &cellDir2, int &cellLth1, int &cellLth2, line_template &sub, signed char &subDir1, signed char &subDir2);
void updateLineSeg(line_template seg_mat[][segNumLimit/2], int seg_cnt[],int &last_idx, bool &emptyFlag, line_template &cell, int &cellCnt, float alpha);
#endif // FREEMAN_H
