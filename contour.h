#ifndef CONTOUR_H
#define CONTOUR_H

#include "common.h"
#define sobelAmpThres 19600
/**
  @brief 用Sobel算子检测图像梯度边缘，非极大值抑制得到细化边缘
*/
void sobel_NMS(uchar *dst, uint *auxSubIndex, uchar *image, ushort width, ushort height, uint sobelThres);
/**
  @brief 二值化图像
*/
void bin2gray(uchar *dst, uchar *image, int size);
/**
  @brief 对图像进行二倍下采样
*/
void downSample_scale2(uchar *dst, uchar *image, uint *preSampleLocat);
#endif // CONTOUR_H
